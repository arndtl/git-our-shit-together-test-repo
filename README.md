# git-our-shit-together-test-repo

This is the toy repository for participants of the _Git our shit together_ workshops.

Slides can be found here: [https://frie.codes/page/gost/](https://frie.codes/page/gost/).

Link to repository with code for slides and installation instructions: [https://gitlab.com/friep/git-our-shit-together](https://gitlab.com/friep/git-our-shit-together)

## Markdown syntax

blub

- **bold**
- _italic_
- ~~strike~~

### subheader

> this is a quote

this links an image from the images folder

![description](images/xkcd_comic.png)

image from the web

![description](https://imgs.xkcd.com/comics/correlation.png)
